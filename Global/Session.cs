﻿using ValhallaEngine.Manager;

namespace ValhallaEngine.Global
{
    public class Session
    {
        private static readonly Session instance = new Session();

        private PhysicsManager physics = null;
        private EntityManager entity = null;
        private RenderManager render = null;
        private AssetManager asset = null;

        private static int globalId = 0;

        static Session() { }
        private Session()
        {
        }

        public static Session Instance
        {
            get => instance;
        }

        public static long NextID
        {
            get
            {
                globalId += 1;
                return globalId;
            }
        }

        public PhysicsManager PhysicsManager { get => physics; set => physics = value; }
        public EntityManager EntityManager { get => entity; set => entity = value; }
        public RenderManager RenderManager { get => render; set => render = value; }
        public AssetManager AssetManager { get => asset; set => asset = value; }
    }
}
