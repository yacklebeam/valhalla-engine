﻿using Microsoft.Xna.Framework;
using System.Collections.Generic;
using ValhallaEngine.Asset;

namespace ValhallaEngine.Component
{
    // Tracks the texture name, the sprite value, etc for a rendered object.
    // Does not store the Texture itself, as this would be copied data
    public class RenderComponent
    {
        private long id;
        private int activeState;
        private int spriteID;
        private float animationTime;
        private float animationTimeMax;
        private Vector2 size;

        public RenderComponent(long id)
        {
            this.id = id;
            activeState = -1;
            spriteID = -1;
            animationTime = 0.25f;
            animationTimeMax = 0.25f;
        }

        public void SetAnimationTime(float t)
        {
            animationTime = t;
            animationTimeMax = t;
        }

        public long Id { get => id; set => id = value; }
        public int ActiveState { get => activeState; set => activeState = value; }
        public int SpriteID { get => spriteID; set => spriteID = value; }
        public float AnimationTime { get => animationTime; set => animationTime = value; }
        public float AnimationTimeMax { get => animationTimeMax; set => animationTimeMax = value; }
        public Vector2 Size { get => size; set => size = value; }
    }
}
