﻿using Microsoft.Xna.Framework;
using ValhallaEngine.Math;

namespace ValhallaEngine.Component
{
    public class PhysicsComponent
    {
        public enum PhysicsType
        {
            NONE,
            COLLIDE,
            INTERSECT
        }

        private Vector2 direction;
        private Vector2 position;
        private Vector2 velocity;
        private Hitbox hitbox = null; // A list of offsets from position
        private long id;
        private Vector2 minBoundingBox;
        private Vector2 maxBoundingBox;
        private PhysicsType type = PhysicsType.NONE;

        public PhysicsComponent(long id)
        {
            this.id = id;
        }

        public long Id { get => id; set => id = value; }
        public Vector2 Position { get => position; set => position = value; }
        public Vector2 Velocity { get => velocity; set => velocity = value; }
        public Vector2 Direction { get => direction; set => direction = value; }
        public Hitbox Hitbox { get => hitbox; set => hitbox = value; }
        public PhysicsType Type { get => type; set => type = value; }


        public Vector2 MinBoundingBox
        {
            get
            {
                minBoundingBox = new Vector2(int.MaxValue, int.MaxValue);
                foreach (Vector2 v in hitbox.PointOffsets)
                {
                    minBoundingBox.X = System.Math.Min(position.X + v.X, minBoundingBox.X);
                    minBoundingBox.Y = System.Math.Min(position.Y + v.Y, minBoundingBox.Y);
                }
                return minBoundingBox;
            }
        }

        public Vector2 MaxBoundingBox
        {
            get
            {
                maxBoundingBox = new Vector2(int.MinValue, int.MinValue);
                foreach (Vector2 v in hitbox.PointOffsets)
                {
                    maxBoundingBox.X = System.Math.Max(position.X + v.X, maxBoundingBox.X);
                    maxBoundingBox.Y = System.Math.Max(position.Y + v.Y, maxBoundingBox.Y);
                }
                return maxBoundingBox;
            }
        }
    }
}
