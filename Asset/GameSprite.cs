﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace ValhallaEngine.Asset
{
    public class GameSprite
    {
        private Texture2D texture;
        private List<Rectangle> states;

        public GameSprite()
        {
            texture = null;
            states = new List<Rectangle>();
        }

        public int GetNextState(int state)
        {
            if (state == -1) return -1;
            int next = state+1;
            next %= states.Count;
            return next;
        }

        public Texture2D Texture { get => texture; set => texture = value; }
        public List<Rectangle> States { get => states; set => states = value; }
    }
}
