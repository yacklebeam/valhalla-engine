﻿using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace ValhallaEngine.Math
{
    public class Hitbox
    {
        private List<Vector2> pointOffsets;

        public Hitbox()
        {
            pointOffsets = new List<Vector2>();
        }

        public List<Vector2> PointOffsets { get => pointOffsets; set => pointOffsets = value; }

        public void Translate(Vector2 v)
        {
            for(int i = 0; i < pointOffsets.Count; ++i)
            {
                pointOffsets[i] = pointOffsets[i] + v;
            }
        }

        public static List<Vector2> ConvertToVerticesInWorld(Hitbox a, Vector2 center)
        {
            List<Vector2> result = new List<Vector2>();
            foreach (Vector2 offset in a.PointOffsets)
            {
                result.Add(center + offset);
            }
            return result;
        }

        public List<Vector2> GetVerticesInWorld(Vector2 center)
        {
            return Hitbox.ConvertToVerticesInWorld(this, center);
        }
    }
}
