﻿using Microsoft.Xna.Framework;
using System.Collections.Generic;
using ValhallaEngine.Component;

namespace ValhallaEngine.Math
{
    class QuadTree
    {
        private QuadTree ul;
        private QuadTree ur;
        private QuadTree ll;
        private QuadTree lr;
        private Vector2 min;
        private Vector2 max;

        private List<PhysicsComponent> components;
        private bool leaf;


        internal QuadTree Ul { get => ul; set => ul = value; }
        internal QuadTree Ur { get => ur; set => ur = value; }
        internal QuadTree Ll { get => ll; set => ll = value; }
        internal QuadTree Lr { get => lr; set => lr = value; }
        public bool Leaf { get => leaf; set => leaf = value; }

        public QuadTree(Vector2 mn, Vector2 mx)
        {
            min = mn;
            max = mx;
            components = new List<PhysicsComponent>();
            leaf = true;
        }

        public bool Insert(PhysicsComponent p)
        {
            if (p.MinBoundingBox.X > min.X && p.MinBoundingBox.Y > min.Y && p.MaxBoundingBox.X <= max.X && p.MaxBoundingBox.Y <= max.Y)
            {
                // This PC is within the BB, check if this will fit in children
                if (components.Count >= 20) // Eventually, refactor this to re-sort components into children
                {
                    if (leaf)
                    {
                        Vector2 mid = MathUtils.MidPoint(min, max);
                        ul = new QuadTree(min, mid);
                        ur = new QuadTree(new Vector2(mid.X, min.Y), new Vector2(max.X, mid.Y));
                        ll = new QuadTree(new Vector2(min.X, mid.Y), new Vector2(mid.X, max.Y));
                        lr = new QuadTree(MathUtils.MidPoint(min, max), max);
                        leaf = false;
                    }

                    if (ul.Insert(p)) return true;
                    if (ur.Insert(p)) return true;
                    if (ll.Insert(p)) return true;
                    if (lr.Insert(p)) return true;

                    components.Add(p); // Children couldn't hold, go ahead and add here -- oh well.
                    return true;
                }
                else
                {
                    components.Add(p);
                    return true;
                }
            }
            else
            {
                return false;
            }
        }

        public List<PhysicsComponent> FindPossibleHits(PhysicsComponent p)
        {
            List<PhysicsComponent> results = new List<PhysicsComponent>();

            if (MathUtils.Intersect(p.MinBoundingBox, p.MaxBoundingBox, min, max)) // P intersects our bounding box, add the components and check the children
            {
                results.AddRange(components);

                if (!leaf)
                {
                    results.AddRange(ul.FindPossibleHits(p));
                    results.AddRange(ur.FindPossibleHits(p));
                    results.AddRange(ll.FindPossibleHits(p));
                    results.AddRange(lr.FindPossibleHits(p));
                }
            }

            return results;
        }
    }
}
