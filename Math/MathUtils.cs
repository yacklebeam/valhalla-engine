﻿using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace ValhallaEngine.Math
{
    public static class MathUtils
    {
        public static bool Intersect(Vector2 aMin, Vector2 aMax, Vector2 bMin, Vector2 bMax)
        {
            if (aMax.X < bMin.X || aMax.Y < bMin.Y) return false;
            if (bMax.X < aMin.X || bMax.Y < aMin.Y) return false;
            return true;
        }

        public static Vector2 MidPoint(Vector2 a, Vector2 b)
        {
            Vector2 r = new Vector2
            {
                X = (a.X + b.X) / 2.0f,
                Y = (a.Y + b.Y) / 2.0f
            };

            return r;
        }

        public static float Distance(Vector2 a, Vector2 b)
        {
            float Xdist = a.X - b.X;
            float Ydist = a.Y - b.Y;

            return (float)System.Math.Sqrt(Xdist * Xdist + Ydist * Ydist);
        }

        public static Hitbox GetRectangleHitbox(Vector2 direction, float length, float width)
        {
            float w = width / 2.0f;
            float l = length / 2.0f;

            direction.Normalize();

            Hitbox h = new Hitbox();
            List<Vector2> hitbox = new List<Vector2>();

            float xMin = -l;
            float yMin = -w;
            float xMax = l;
            float yMax = w;

            hitbox.Add(new Vector2(xMin, yMin));
            hitbox.Add(new Vector2(xMax, yMin));
            hitbox.Add(new Vector2(xMax, yMax));
            hitbox.Add(new Vector2(xMin, yMax));

            h.PointOffsets = hitbox;

            return h;
        }

        public struct CollisionResult
        {
            float t;
            Vector2 normal;

            public float T { get => t; set => t = value; }
            public Vector2 Normal { get => normal; set => normal = value; }
        }

        // COLLISION CODE STARTS HERE

        public static float Cross(Vector2 a, Vector2 b)
        {
            return (a.X * b.X) + (a.Y * b.Y);
        }

        private static int FindMax(List<Vector2> A, Vector2 D)
        {
            float maxValue = float.MinValue;
            int maxI = -1;

            for (int i = 0; i < A.Count; ++i)
            {
                float value = Cross(D, A[i]);
                if (value > maxValue)
                {
                    maxValue = value;
                    maxI = i;
                }
            }

            return maxI;
        }

        private static bool IsAligned(Vector2 X, Vector2 Y)
        {
            return Cross(X, Y) >= 0;
        }

        private static Vector2 GetPerpindicularFrom(Vector2 D, Vector2 dAway)
        {
            Vector2 result = Vector2.Zero;
            result.Y = D.X;
            result.X = -D.Y;
            if (IsAligned(result, dAway))
            {
                result = result * -1.0f;
            }
            return result;
        }

        private static Vector2 Support(List<Vector2> X, List<Vector2> Y, Vector2 D)
        {
            Vector2 result = Vector2.Zero;
            int indexX = FindMax(X, D);
            int indexY = FindMax(Y, D * -1.0f);
            result = X[indexX] - Y[indexY];
            return result;
        }

        private static bool AroundLine(Vector2 X, Vector2 Y, Vector2 line)
        {
            float A = line.Y;
            float B = line.X * -1.0f;

            float xResult = A * X.X + B * X.Y;
            float yResult = A * Y.X + B * Y.Y;

            if (xResult == 0 || yResult == 0) return false;
            if (xResult < 0 && yResult > 0) return true;
            if (xResult > 0 && yResult < 0) return true;
            else return false;
        }

        private static float GetIntersection(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4)
        {
            float Result = 0.0f;
            float Numerator = ((p4.X - p3.X) * (p1.Y - p3.Y)) - ((p4.Y - p3.Y) * (p1.X - p3.X));
            float Denominator = ((p4.Y - p3.Y) * (p2.X - p1.X)) - ((p4.X - p3.X) * (p2.Y - p1.Y));
            if (Denominator == 0)
            {
                return 1.0f;
            }
            Result = Numerator / Denominator;
            return Result;
        }

        /// <summary>
        /// Performs a collision check against two polygons, one of which is moving
        /// </summary>
        /// <param name="X">The polygon that is moving</param>
        /// <param name="Y">The polygon that is stationary</param>
        /// <param name="D">The movement vector X is attempting</param>
        /// <returns></returns>
        public static CollisionResult GetCollision(List<Vector2> X, List<Vector2> Y, Vector2 D)
        {
            // TPA / GJK Penetrating Collision Test
            List<Vector2> simplex = new List<Vector2>();
            Vector2 A = Support(X, Y, D);
            simplex.Add(A);

            Vector2 perpD = GetPerpindicularFrom(D, A);
            Vector2 B = Support(X, Y, perpD);
            simplex.Add(B);

            int count = 0;
            bool infinite = false;
            bool exit = false;

            while (count < (X.Count * Y.Count))
            {
                Vector2 newDir = GetPerpindicularFrom(simplex[1] - simplex[0], D * -1.0f);
                Vector2 C = Support(X, Y, newDir);
                if (C == simplex[0] || C == simplex[1])
                {
                    // C is already in the list, Simplex is complete
                    exit = true;
                }
                if (AroundLine(simplex[0], C, D * -1.0f))
                {
                    simplex[1] = C;
                }
                else if (AroundLine(simplex[1], C, D * -1.0f))
                {
                    simplex[0] = C;
                }
                else
                {
                    // No intersection
                    infinite = true;
                    break;
                }
                ++count;
                if (exit) break;
            }

            CollisionResult result = new CollisionResult();

            Vector2 surfaceNormal = GetPerpindicularFrom(simplex[0] - simplex[1], D);
            surfaceNormal.Normalize();
            result.Normal = surfaceNormal;

            if (!infinite)
            {
                //instead of using line, we use the parallel line kEpsilon away from the line
                float testT = GetIntersection(Vector2.Zero, D * -1.0f, simplex[0], simplex[1]);
                result.T = testT;
                Vector2 k_EpsilonVector = result.Normal * (0.001f);
                Vector2 point1 = simplex[0] - k_EpsilonVector;
                Vector2 point2 = simplex[1] - k_EpsilonVector;

                result.T = GetIntersection(Vector2.Zero, D * -1.0f, point1, point2);
                if (testT > 0.0f && result.T < 0.0f)
                {
                    result.T = 0.0f;
                }
            }
            else
            {
                result.T = 1.0f;
            }

            return result;
        }
    }
}
