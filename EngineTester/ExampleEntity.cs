﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using ValhallaEngine.Entity;
using ValhallaEngine.Component;
using ValhallaEngine.Global;

namespace EngineTester
{
    class ExampleEntity : GameEntity
    {
        private double cooldown;

        public ExampleEntity(long id) : base(id)
        {
            cooldown = 1.0;
        }

        public override void OnEvent(long id)
        {

        }

        public override void OnUpdate(GameTime t)
        {
            cooldown -= t.ElapsedGameTime.TotalSeconds;
            if(cooldown <= 0.0)
            {
                cooldown += 1.0;
            }
        }
    }
}
