﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ValhallaEngine.Manager;
using ValhallaEngine.Component;
using ValhallaEngine.Asset;
using ValhallaEngine.Global;

namespace EngineTester
{
    public class EngineTesterGame : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        System.Random rand;
        int stationaryCount;
        bool paused = false;
        double renderTime;

        public EngineTesterGame()
        {
            //IsFixedTimeStep = false;
            
            graphics = new GraphicsDeviceManager(this);
            graphics.IsFullScreen = false;
            graphics.PreferredBackBufferWidth = 360;
            graphics.PreferredBackBufferHeight = 270;
            //graphics.SynchronizeWithVerticalRetrace = false;
            Content.RootDirectory = "Content";

            stationaryCount = 0;
            renderTime = 0.0;
        }

        protected override void Initialize()
        {
            Session.Instance.AssetManager = new AssetManager();
            Session.Instance.RenderManager = new RenderManager();
            Session.Instance.EntityManager = new EntityManager();
            Session.Instance.PhysicsManager = new PhysicsManager(new Vector2(GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height), new Vector2(48.0f, 36.0f));
            rand = new System.Random();
            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            Session.Instance.AssetManager.LoadImageAsset("example", "example-spritesheet", Content);
            Session.Instance.AssetManager.LoadImageAsset("wall", "wall", Content);
            Session.Instance.AssetManager.LoadFontAsset("debug", "DebugFont", Content);

            GameSprite gs = new GameSprite();
            gs.Texture = Session.Instance.AssetManager.GetTexture("example");
            gs.States.Add(new Rectangle( 0, 0, 20, 20));
            gs.States.Add(new Rectangle(20, 0, 20, 20));
            gs.States.Add(new Rectangle(40, 0, 20, 20));
            gs.States.Add(new Rectangle(60, 0, 20, 20));
            Session.Instance.AssetManager.AddGameSprite(0, gs);

            Session.Instance.AssetManager.CreateGameSprite(1, "wall", null);

            /*ExampleEntity ex = new ExampleEntity(Session.NextID);
            PhysicsComponent expc = new PhysicsComponent(ex.Id);
            RenderComponent exrc = new RenderComponent(ex.Id);
            expc.Hitbox = ValhallaEngine.Math.MathUtils.GetRectangleHitbox(new Vector2(1, 0), 1, 1);
            expc.Type = PhysicsComponent.PhysicsType.COLLIDE;
            expc.Velocity = new Vector2(5, 5);
            expc.Position = new Vector2(12, 12);
            exrc.SpriteID = 1;
            exrc.Size = new Vector2(1, 1);
            exrc.SetAnimationTime(0.25f);
            gameSession.PhysicsManager.Add(expc);
            gameSession.RenderManager.Add(exrc);
            gameSession.EntityManager.Add(ex);*/

            //BuildWall(new Vector2(0, 0), new Vector2(1, 0), 48);
            //BuildWall(new Vector2(0, 0), new Vector2(0, 1), 36);
            //BuildWall(new Vector2(0, 35), new Vector2(1, 0), 48);
            //BuildWall(new Vector2(47, 0), new Vector2(0, 1), 36);
        }

        private void GenerateRandomEntity()
        {
            Vector2 position = new Vector2(rand.Next(10, 38), rand.Next(10, 26));
            Vector2 velocity = new Vector2(0.1f * rand.Next(-1, 2), 0.1f * rand.Next(-1, 2));

            if (velocity == Vector2.Zero) stationaryCount++;

            ExampleEntity ex = new ExampleEntity(Session.NextID);
            PhysicsComponent expc = new PhysicsComponent(ex.Id);
            RenderComponent exrc = new RenderComponent(ex.Id);
            expc.Hitbox = ValhallaEngine.Math.MathUtils.GetRectangleHitbox(new Vector2(1,0), 1, 1);
            expc.Type = PhysicsComponent.PhysicsType.NONE;
            expc.Velocity = velocity;
            expc.Position = position;
            exrc.SpriteID = 1;
            exrc.Size = new Vector2(1, 1);
            exrc.SetAnimationTime(0.25f);
            Session.Instance.PhysicsManager.Add(expc);
            Session.Instance.RenderManager.Add(exrc);
            Session.Instance.EntityManager.Add(ex);
        }

        private void BuildWall(Vector2 start, Vector2 spacing, int count)
        {
            for(int i = 0; i < count; ++i)
            {
                AddWall(start + (spacing * i));
            }
        }

        private void AddWall(Vector2 position)
        {
            ExampleWall ex = new ExampleWall(Session.NextID);
            PhysicsComponent expc = new PhysicsComponent(ex.Id);
            RenderComponent exrc = new RenderComponent(ex.Id);
            expc.Hitbox = null;// ValhallaEngine.Math.MathUtils.GetRectangleHitbox(new Vector2(1, 0), 1, 1);
            expc.Type = PhysicsComponent.PhysicsType.COLLIDE;
            expc.Position = position;
            exrc.SpriteID = 1;
            exrc.Size = new Vector2(1, 1);
            Session.Instance.PhysicsManager.Add(expc);
            Session.Instance.RenderManager.Add(exrc);
            Session.Instance.EntityManager.Add(ex);
        }

        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            renderTime = gameTime.ElapsedGameTime.TotalMilliseconds;
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            if (!paused)
            {
                GenerateRandomEntity();

                Session.Instance.PhysicsManager.Update(gameTime, Session.Instance.EntityManager);
                Session.Instance.EntityManager.Update(gameTime);
            }

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            spriteBatch.Begin(SpriteSortMode.Deferred, null, SamplerState.PointClamp, null, null, null);
            Session.Instance.RenderManager.Render(gameTime, spriteBatch, Session.Instance.PhysicsManager, Session.Instance.AssetManager);
            string entCounts = string.Format("{0} ({1})", Session.Instance.EntityManager.Count.ToString(), stationaryCount.ToString());
            string elapsedTime = string.Format("{0}ms", gameTime.ElapsedGameTime.TotalMilliseconds.ToString());
            string elapsedRTime = string.Format("{0}ms", renderTime.ToString());
            spriteBatch.DrawString(Session.Instance.AssetManager.GetFont("debug"), entCounts, new Vector2(10, 10), Color.White);
            spriteBatch.DrawString(Session.Instance.AssetManager.GetFont("debug"), elapsedTime, new Vector2(10, 30), Color.White);
            spriteBatch.DrawString(Session.Instance.AssetManager.GetFont("debug"), elapsedRTime, new Vector2(10, 50), Color.White);
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
