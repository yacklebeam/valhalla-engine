﻿using Microsoft.Xna.Framework;
using ValhallaEngine.Entity;

namespace EngineTester
{
    class ExampleWall : GameEntity
    {
        public ExampleWall(long id) : base(id)
        {
        }

        public override void OnEvent(long id)
        {
        }

        public override void OnUpdate(GameTime t)
        {
        }
    }
}
