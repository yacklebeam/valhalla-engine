﻿using ValhallaEngine.Component;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using ValhallaEngine.Math;
using ValhallaEngine.Entity;

namespace ValhallaEngine.Manager
{
    public class PhysicsManager
    {
        private Vector2 maxPixelViewport;
        private Vector2 maxMeterViewport;

        private Vector2 cameraOffset;

        private Dictionary<long, PhysicsComponent> components;

        public PhysicsManager(Vector2 pixels, Vector2 meters)
        {
            maxPixelViewport = pixels;
            maxMeterViewport = meters;
            cameraOffset = new Vector2();
            components = new Dictionary<long, PhysicsComponent>();
        }

        public PhysicsComponent Get(long id)
        {
            return components[id];
        }

        public void Add(PhysicsComponent p)
        {
            components.Add(p.Id, p);
        }

        public void Update(GameTime t, EntityManager e)
        {
            foreach (PhysicsComponent p in components.Values)
            {
                if(p.Velocity.LengthSquared() > 0.001)
                {
                    Vector2 originalPos = p.Position;
                    p.Position += p.Velocity * (float)t.ElapsedGameTime.TotalSeconds;
                    // If necessary, call TriggerEvent();

                    if (p.Hitbox == null) continue;

                    //List<long> pcs = GetIntersections(p.Id);

                    /*foreach (long otherId in pcs)
                    {
                        PhysicsComponent other = Get(otherId);
                        GameEntity ge = e.Get(p.Id);
                        if (p.Type == PhysicsComponent.PhysicsType.COLLIDE && p.Type == PhysicsComponent.PhysicsType.COLLIDE)
                        {// We need to adjust our movement
                            p.Position = originalPos; // For now, just prevent the movement.  Eventually, calculate the pen distance

                            // Also, trigger our event
                            if(ge != null) ge.OnEvent(otherId);
                        }
                        else if (p.Type == PhysicsComponent.PhysicsType.INTERSECT)
                        {// We intersected the object. trigger our event
                            if(ge != null) ge.OnEvent(otherId);
                        }
                    }*/
                }
            }
        }

        public void Delete(long id)
        {
            if (id > -1) components.Remove(id);
        }

        public List<long> GetIntersections(long id)
        {
            List<long> pcs = new List<long>();

            PhysicsComponent pc = components[id];
            int i = 0;
            foreach (PhysicsComponent other in components.Values)
            {
                if (other.Id != id)
                {
                    if (MathUtils.Intersect(pc.MinBoundingBox, pc.MaxBoundingBox, other.MinBoundingBox, other.MaxBoundingBox))
                    {
                        pcs.Add(other.Id);
                    }
                }
                // Only get first 10 for benchmarking
                i++;
                if (i > 20) break;
            }

            return pcs;
        }

        public Vector2 MaxPixelViewport { get => maxPixelViewport; set => maxPixelViewport = value; }
        public Vector2 CameraOffset { get => cameraOffset; set => cameraOffset = value; }
        public Vector2 MaxMeterViewport { get => maxMeterViewport; set => maxMeterViewport = value; }

        public Vector2 ConvertToScreenCoordinates(Vector2 v)
        {
            Vector2 Result = new Vector2();

            float pixelsPerMeter = (maxPixelViewport.X) / (maxMeterViewport.X);

            Result.X = (float)System.Math.Floor((v.X - cameraOffset.X) * pixelsPerMeter);
            Result.Y = (float)System.Math.Floor((v.Y - cameraOffset.Y) * pixelsPerMeter);

            return Result;
        }
    }
}
