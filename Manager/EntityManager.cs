﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ValhallaEngine.Entity;

namespace ValhallaEngine.Manager
{
    public class EntityManager
    {
        private readonly Dictionary<long, GameEntity> entites;
        private readonly Dictionary<long, GameEntity> newEntities;
        private readonly List<long> deletedEntities;
        private long playerId;

        public long PlayerId { get => playerId; set => playerId = value; }

        public EntityManager()
        {
            playerId = -1;
            newEntities = new Dictionary<long, GameEntity>();
            entites = new Dictionary<long, GameEntity>();
            deletedEntities = new List<long>();
        }

        public int Count
        {
            get
            {
                return entites.Count;
            }
        }

        public GameEntity Get(long id)
        {
            try
            {
                return entites[id];
            }
            catch (KeyNotFoundException)
            {
                return null;
            }
        }

        public void Update(GameTime t)
        {
            foreach (GameEntity e in entites.Values)
            {
                e.Update(t); // Entity Update
            }

            foreach (GameEntity g in newEntities.Values)
            {
                entites.Add(g.Id, g);
            }

            foreach (long i in deletedEntities)
            {
                entites.Remove(i);
            }

            newEntities.Clear();
            deletedEntities.Clear();
        }

        public void LoadEntity(GameEntity e)
        {
            entites.Add(e.Id, e);
        }

        public void Add(GameEntity e)
        {
            newEntities.Add(e.Id, e);
        }

        public void Delete(long id)
        {
            if (id > -1) deletedEntities.Add(id);
        }
    }
}
