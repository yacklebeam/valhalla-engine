﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using ValhallaEngine.Asset;

namespace ValhallaEngine.Manager
{
    public class AssetManager
    {
        Dictionary<string, Texture2D> textures;
        Dictionary<string, SpriteFont> fonts;
        Dictionary<string, SoundEffect> sounds;
        Dictionary<int, GameSprite> sprites;

        public AssetManager()
        {
            textures = new Dictionary<string, Texture2D>();
            fonts = new Dictionary<string, SpriteFont>();
            sounds = new Dictionary<string, SoundEffect>();
            sprites = new Dictionary<int, GameSprite>();
        }

        public void AddGameSprite(int id, GameSprite g)
        {
            sprites.Add(id, g);
        }

        public void CreateGameSprite(int id, Texture2D tex, List<Rectangle> states)
        {
            GameSprite gs = new GameSprite();
            gs.Texture = tex;
            if (states == null) gs.States = new List<Rectangle>();
            else gs.States = states;
            sprites.Add(id, gs);
        }

        public void CreateGameSprite(int id, string texName, List<Rectangle> states)
        {
            GameSprite gs = new GameSprite();
            gs.Texture = GetTexture(texName);
            if (states == null) gs.States = new List<Rectangle>();
            else gs.States = states;
            sprites.Add(id, gs);
        }

        public GameSprite GetGameSprite(int id)
        {
            if (sprites.ContainsKey(id)) return sprites[id];
            else return null;
        }

        public void LoadImageAsset(string imageId, string imagePath, ContentManager content)
        {
            if (!textures.ContainsKey(imageId))
            {
                Texture2D newTexture = content.Load<Texture2D>(imagePath);
                textures.Add(imageId, newTexture);
            }
        }

        public void LoadFontAsset(string fontId, string fontPath, ContentManager content)
        {
            if (!fonts.ContainsKey(fontId))
            {
                SpriteFont newFont = content.Load<SpriteFont>(fontPath);
                fonts.Add(fontId, newFont);
            }
        }

        public void LoadSoundAsset(string soundId, string soundPath, ContentManager content)
        {
            if (!sounds.ContainsKey(soundId))
            {
                SoundEffect newSound = content.Load<SoundEffect>(soundPath);
                sounds.Add(soundId, newSound);
            }
        }

        public Texture2D GetTexture(string textureId)
        {
            return textures[textureId];
        }

        public SpriteFont GetFont(string fontId)
        {
            return fonts[fontId];
        }

        public SoundEffect GetSound(string soundId)
        {
            return sounds[soundId];
        }
    }
}
