﻿using System.Collections.Generic;
using ValhallaEngine.Component;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ValhallaEngine.Asset;

namespace ValhallaEngine.Manager
{
    public class RenderManager
    {
        private readonly Dictionary<long, RenderComponent> renders;

        public RenderManager()
        {
            renders = new Dictionary<long, RenderComponent>();
        }

        public RenderComponent Get(long id)
        {
            return renders[id];
        }

        public void Add(RenderComponent r)
        {
            renders.Add(r.Id, r);
        }

        public void Render(GameTime t, SpriteBatch sb, PhysicsManager p, AssetManager a)
        {
            foreach (RenderComponent r in renders.Values)
            {
                GameSprite gs = a.GetGameSprite(r.SpriteID);
                if (gs != null)
                {
                    Vector2 position = p.ConvertToScreenCoordinates(p.Get(r.Id).Position);
                    Vector2 size = p.ConvertToScreenCoordinates(r.Size);
                    if (gs.States.Count > r.ActiveState && r.ActiveState != -1)
                    {
                        sb.Draw(gs.Texture,
                                new Rectangle((int)position.X, (int)position.Y, (int)size.X, (int)size.Y),
                                gs.States[r.ActiveState], Color.White);
                    }
                    else
                    {
                        sb.Draw(gs.Texture, new Rectangle((int)position.X, (int)position.Y, (int)size.X, (int)size.Y), Color.White);
                    }
                    r.AnimationTime -= (float)t.ElapsedGameTime.TotalSeconds;
                    if(r.AnimationTime <= 0)
                    {
                        r.ActiveState = gs.GetNextState(r.ActiveState);
                        r.AnimationTime += r.AnimationTimeMax;
                    }
                }
            }
        }

        public void Delete(long id)
        {
            if (id > -1) renders.Remove(id);
        }
    }
}
